package com.thoriqgamehouse.gameservice.dto;

import java.util.List;

public class PlayerStatsResponse {
    private long playerId;
    private long GameCount;
    private List<GameDTO> games;
    private GameDTO recentlyPlayedGame;
}
