package com.thoriqgamehouse.gameservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class EventResponse {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Metadata metadata;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<EventDTO> events;
}
