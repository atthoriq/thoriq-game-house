package com.thoriqgamehouse.gameservice.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Metadata {
    private int statusCode;
    private String message;
}
