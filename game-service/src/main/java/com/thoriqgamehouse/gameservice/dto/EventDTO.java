package com.thoriqgamehouse.gameservice.dto;

import lombok.Builder;
import lombok.Getter;

import java.sql.Timestamp;

@Getter
@Builder
public class EventDTO {
    private long eventId;
    private long gameId;
    private String eventName;
    private String gameName;
    private Timestamp startDate;
    private Timestamp endDate;
}
