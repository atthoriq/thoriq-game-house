package com.thoriqgamehouse.gameservice.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PlayerDTO {
    private long playerId;
    private long gameId;
    private long userId;
    private long level;
    private String rank;
    private long winCount;
    private long loseCount;
}
