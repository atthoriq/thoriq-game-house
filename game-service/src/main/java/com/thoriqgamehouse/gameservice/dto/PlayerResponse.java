package com.thoriqgamehouse.gameservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.sql.Timestamp;

@Getter
@Builder
public class PlayerResponse {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Metadata metadata;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private PlayerDTO player;
}
