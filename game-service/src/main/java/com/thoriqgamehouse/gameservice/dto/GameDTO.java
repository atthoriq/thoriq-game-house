package com.thoriqgamehouse.gameservice.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GameDTO {
    private long id;
    private long name;
}
