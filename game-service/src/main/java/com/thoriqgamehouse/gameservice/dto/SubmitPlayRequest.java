package com.thoriqgamehouse.gameservice.dto;

import com.thoriqgamehouse.gameservice.model.PlayerItemLog;
import jakarta.persistence.Transient;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Getter
@Builder
public class SubmitPlayRequest {
    private long playerId;
    private long gameId;
    private long eventId;
    private int winStatus;
    private long gameDurationInS;
    private List<PlayerItemLog> itemUsed;
}
