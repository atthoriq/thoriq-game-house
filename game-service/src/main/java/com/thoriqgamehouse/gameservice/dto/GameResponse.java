package com.thoriqgamehouse.gameservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GameResponse {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Metadata metadata;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private GameDTO game;
}
