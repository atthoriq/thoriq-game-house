package com.thoriqgamehouse.gameservice.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UpdatePlayerRequest {
    private long playerId;
    private long level;
    private String rank;
    private long winCount;
    private long loseCount;
}
