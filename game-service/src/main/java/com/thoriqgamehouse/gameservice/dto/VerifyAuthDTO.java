package com.thoriqgamehouse.gameservice.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class VerifyAuthDTO {
    private String authToken;
}
