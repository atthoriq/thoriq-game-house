package com.thoriqgamehouse.gameservice.repository;

import com.thoriqgamehouse.gameservice.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {
    List<Player> findByGameIdAndUserId(long gameId, long userId);

    @Modifying
    @Transactional
    @Query(
            value = "update player set level = level + ?1 where id = ?2",
            nativeQuery = true
    )
    void addLevelBy(long level, long playerId);

    @Modifying
    @Transactional
    @Query(
            value = "update player set win_count = win_count + 1 where id = ?1",
            nativeQuery = true
    )
    void incrementWinCount(long playerId);

    @Modifying
    @Transactional
    @Query(
            value = "update player set lose_count = lose_count + 1 where id = ?1",
            nativeQuery = true
    )
    void incrementLoseCount(long playerId);
}
