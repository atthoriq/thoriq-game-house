package com.thoriqgamehouse.gameservice.repository;

import com.thoriqgamehouse.gameservice.model.RunningEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RunningEventRepository extends CrudRepository<RunningEvent, Long> {
}
