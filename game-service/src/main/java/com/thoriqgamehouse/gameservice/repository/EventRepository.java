package com.thoriqgamehouse.gameservice.repository;

import com.thoriqgamehouse.gameservice.model.Event;
import jakarta.annotation.Resource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Long>{

    @Query(value = "select * from event e where e.game_id= ?1 and e.start_date <= now() and e.end_date > now()", nativeQuery = true)
    List<Event> getEventRunning(long gameId);

}
