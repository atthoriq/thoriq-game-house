package com.thoriqgamehouse.gameservice.repository;

import com.thoriqgamehouse.gameservice.model.PlayerLog;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerLogRepository extends MongoRepository<PlayerLog, String> {
}
