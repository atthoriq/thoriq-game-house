package com.thoriqgamehouse.gameservice.controller;

import com.thoriqgamehouse.gameservice.dto.*;
import com.thoriqgamehouse.gameservice.model.Player;
import com.thoriqgamehouse.gameservice.model.PlayerLog;
import com.thoriqgamehouse.gameservice.service.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class PlayerController {
    private final PlayerService playerService;

    @GetMapping("/player")
    public ResponseDTO<PlayerResponse> getPlayer(@RequestParam long gameId, @RequestParam long userId) {
        try {
            Player player = playerService.getPlayer(gameId, userId);
            PlayerDTO dto = PlayerDTO.builder()
                    .playerId(player.getId())
                    .userId(player.getUserId())
                    .level(player.getLevel())
                    .gameId(player.getGame().getId())
                    .rank(player.getRank())
                    .winCount(player.getWinCount())
                    .loseCount(player.getLoseCount())
                    .build();

            return ResponseDTO.ok(PlayerResponse.builder()
                            .player(dto)
                            .build());
        } catch (IllegalAccessException e){
            return ResponseDTO.badRequest(PlayerResponse.builder()
                            .metadata(Metadata.builder()
                                    .statusCode(400)
                                    .message("data not found")
                                    .build())
                            .build(), "data not found");
        } catch (RuntimeException e) {
            return ResponseDTO.badRequest(PlayerResponse.builder()
                    .metadata(Metadata.builder()
                            .statusCode(500)
                            .message("unexpected error")
                            .build())
                    .build(), "unexpected error");
        }
    }

    @PutMapping("/player/{playerId}/level/add")
    public ResponseDTO<Metadata> addPlayerLevelBy(@RequestParam long levelAdd, @PathVariable long playerId) {
        playerService.addPlayerLevelBy(levelAdd, playerId);

        return ResponseDTO.ok(Metadata.builder()
                .statusCode(200)
                .message("Success")
                .build());
    }

    @PostMapping("/play/submit")
    public ResponseDTO<Metadata> submitPlay(@RequestBody @Validated SubmitPlayRequest request) {
        playerService.submitPlay(PlayerLog.builder()
                        .playerId(request.getPlayerId())
                        .gameId(request.getGameId())
                        .eventId(request.getEventId())
                        .gameDurationInS(request.getGameDurationInS())
                        .winStatus(request.getWinStatus())
                        .itemUsed(request.getItemUsed())
                        .build());

        return ResponseDTO.ok(Metadata.builder()
                        .statusCode(200)
                        .message("Success")
                .build());
    }
}
