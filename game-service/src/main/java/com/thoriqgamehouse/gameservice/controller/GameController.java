package com.thoriqgamehouse.gameservice.controller;

import com.thoriqgamehouse.gameservice.dto.EventDTO;
import com.thoriqgamehouse.gameservice.dto.EventResponse;
import com.thoriqgamehouse.gameservice.dto.ResponseDTO;
import com.thoriqgamehouse.gameservice.model.Event;
import com.thoriqgamehouse.gameservice.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class GameController {
    private final GameService gameService;

    @GetMapping("/game/{gameId}")
    public ResponseDTO<EventResponse> getRunningEvent(
            @PathVariable String gameId
    ) {
        List<Event> events = gameService.getRunningEvent(Long.valueOf(gameId));

        if(events.isEmpty()) {
            return ResponseDTO.ok(EventResponse.builder()
                    .events(List.of())
                    .build());
        }

        List<EventDTO> eventRespons = new ArrayList<>();

        for(Event e: events) {
            eventRespons.add(EventDTO.builder()
                            .eventId(e.getId())
                            .gameId(e.getGame().getId())
                            .eventName(e.getName())
                            .gameName(e.getGame().getName())
                            .startDate(e.getStartDate())
                            .endDate(e.getEndDate())
                            .build());
        }
        return ResponseDTO.ok(EventResponse.builder()
                .events(eventRespons)
                .build());
    }
}
