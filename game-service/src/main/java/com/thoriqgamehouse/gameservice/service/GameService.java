package com.thoriqgamehouse.gameservice.service;

import com.thoriqgamehouse.gameservice.model.Event;
import com.thoriqgamehouse.gameservice.model.Game;
import com.thoriqgamehouse.gameservice.model.RunningEvent;
import com.thoriqgamehouse.gameservice.model.RunningEventGame;
import com.thoriqgamehouse.gameservice.repository.EventRepository;
import com.thoriqgamehouse.gameservice.repository.RunningEventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GameService {
    private final EventRepository eventRepository;
    private final RunningEventRepository runningEventRepository;

    public List<Event> getRunningEvent(long gameId) {
        List<RunningEvent> res = (List<RunningEvent>) runningEventRepository.findAllById(List.of(gameId));
        if (res.isEmpty()) {
            List<Event> events = eventRepository.getEventRunning(gameId);

            List<RunningEvent> runningEvents = new ArrayList<>();
            for (Event ev: events) {
                runningEvents.add(RunningEvent.builder()
                                .game(RunningEventGame.builder()
                                        .id(ev.getGame().getId())
                                        .name(ev.getGame().getName())
                                        .description(ev.getGame().getDescription())
                                        .build())
                                .id(ev.getId())
                                .startDate(ev.getStartDate())
                                .endDate(ev.getEndDate())
                                .name(ev.getName())
                                .build());
            }
            runningEventRepository.saveAll(runningEvents);

            return events;
        }

        List<Event> events = new ArrayList<>();
        for (RunningEvent re: res) {
            events.add(Event.builder()
                            .game(Game.builder()
                                    .id(re.getGame().getId())
                                    .name(re.getGame().getName())
                                    .description(re.getGame().getDescription())
                                    .build())
                            .id(re.getId())
                            .name(re.getName())
                            .startDate(re.getStartDate())
                            .endDate(re.getEndDate())
                    .build());
        }
        return events;
    }
}
