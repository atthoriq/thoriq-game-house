package com.thoriqgamehouse.gameservice.service;

import com.thoriqgamehouse.gameservice.dto.PlayerStatsResponse;
import com.thoriqgamehouse.gameservice.dto.UpdatePlayerRequest;
import com.thoriqgamehouse.gameservice.model.Player;
import com.thoriqgamehouse.gameservice.model.PlayerLog;
import com.thoriqgamehouse.gameservice.repository.PlayerLogRepository;
import com.thoriqgamehouse.gameservice.repository.PlayerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PlayerService {
    private final PlayerRepository playerRepository;
    private final PlayerLogRepository playerLogRepository;

    public Player getPlayer(long gameId, long userId) throws RuntimeException, IllegalAccessException {
        List<Player> players = playerRepository.findByGameIdAndUserId(gameId, userId);
        if (players.isEmpty()) {
            throw new IllegalAccessException();
        }

        if (players.size() > 1) {
            throw new RuntimeException();
        }

        return players.get(0);
    }

    public void addPlayerLevelBy(long levelAdd, long playerId) {
        playerRepository.addLevelBy(levelAdd, playerId);
    }

    public PlayerStatsResponse getPlayerStats(long playerId) {
        return null;
    }

    @Transactional
    public void submitPlay(PlayerLog log) {
        if (log.getWinStatus() == 1) {
            playerRepository.incrementWinCount(log.getPlayerId());
        } else {
            playerRepository.incrementLoseCount(log.getPlayerId());
        }

        this.savePlayerLog(log);
    }

    private void savePlayerLog(PlayerLog log) {
        log.setPlayedAt(Instant.now().toString());
        playerLogRepository.save(log);
    }
}
