package com.thoriqgamehouse.gameservice;

import com.thoriqgamehouse.gameservice.repository.EventRepository;
import com.thoriqgamehouse.gameservice.repository.GameRepository;
import com.thoriqgamehouse.gameservice.repository.PlayerLogRepository;
import com.thoriqgamehouse.gameservice.repository.PlayerRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = PlayerLogRepository.class)
@EnableJpaRepositories(basePackageClasses = {EventRepository.class, GameRepository.class, PlayerRepository.class})
@EnableCaching
@SpringBootApplication
public class GameServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameServiceApplication.class, args);
	}

}
