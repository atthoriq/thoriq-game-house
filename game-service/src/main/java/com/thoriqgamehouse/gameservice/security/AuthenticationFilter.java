package com.thoriqgamehouse.gameservice.security;

import com.thoriqgamehouse.gameservice.config.AppConfig;
import com.thoriqgamehouse.gameservice.dto.VerifyAuthDTO;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;


@Component
@RequiredArgsConstructor
public class AuthenticationFilter implements Filter {

    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    AppConfig appConfig;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;
        final String authHeader = request.getHeader("Authorization");
        if ("OPTIONS".equals(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
            filterChain.doFilter(request, response);
        } else {
            if(authHeader == null || !authHeader.startsWith("Bearer ")){
                throw new ServletException("An exception occurred");
            }
        }
        String token = authHeader.substring(7);
        if(!this.isAuthenticated(token)) {
            throw new ServletException("An exception occurred");
        };
        filterChain.doFilter(request, response);
    }

    public boolean isAuthenticated(String authHeader) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(authHeader);

        VerifyAuthDTO v = VerifyAuthDTO.builder().authToken(authHeader).build();
        HttpEntity<VerifyAuthDTO> request = new HttpEntity<>(v, headers);

        String url = appConfig.getAuthVerificationHost() + appConfig.getAuthVerificationUri();

        ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        if(result.getStatusCode() == HttpStatus.OK) {
            return true;
        }

        return false;
    }


}
