package com.thoriqgamehouse.gameservice.config;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

@Component
@ReadingConverter
public class BytesToOffsetDateTimeConverter implements Converter<byte[], Timestamp> {
    @Override
    public Timestamp convert(final byte[] source) {
//        return OffsetDateTime.parse(new String(source), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        String value = new String(source);
        return new Timestamp(Long.parseLong(value));
    }
}
