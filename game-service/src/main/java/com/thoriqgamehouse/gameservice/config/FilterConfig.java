package com.thoriqgamehouse.gameservice.config;

import com.thoriqgamehouse.gameservice.security.AuthenticationFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

//    @Bean
    public FilterRegistrationBean<AuthenticationFilter> jwtFilter() {
        FilterRegistrationBean<AuthenticationFilter> filter= new FilterRegistrationBean<>();
        filter.setFilter(new AuthenticationFilter());

        filter.addUrlPatterns("/*");
        return filter;
    }
}