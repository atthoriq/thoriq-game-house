package com.thoriqgamehouse.gameservice.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PlayerItemLog {
    private String itemName;
    private String itemType;
}
