package com.thoriqgamehouse.gameservice.model;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.redis.core.index.Indexed;

@Getter
@Builder
public class RunningEventGame {
    @Indexed
    private long id;
    private String name;
    private String description;
}
