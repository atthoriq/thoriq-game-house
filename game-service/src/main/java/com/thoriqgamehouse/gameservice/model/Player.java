package com.thoriqgamehouse.gameservice.model;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.ZonedDateTime;

@Entity
@Table(name = "player")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Player {
    @Id
    private long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "game_id")
    private Game game;

    private long userId;

    private long level;

    private String rank;

    private long winCount;

    private long loseCount;

    @Column(name = "updated_at")
    private Timestamp updatedAt;
}
