package com.thoriqgamehouse.gameservice.model;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Document(collection = "player_log")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerLog {
    @Id
    private String id;
    private long playerId;
    private long gameId;
    private long eventId;
    private String playedAt;
    private int winStatus;
    private long gameDurationInS;
    @Transient
    private List<PlayerItemLog> itemUsed;
}
