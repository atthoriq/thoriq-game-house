package com.thoriqgamehouse.gameservice.model;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
@Builder
@RedisHash(value = "RunningEvent", timeToLive = 300)
public class RunningEvent implements Serializable {
    @Indexed
    private long id;

    private RunningEventGame game;

    private String name;

    private Timestamp startDate;

    private Timestamp endDate;
}
