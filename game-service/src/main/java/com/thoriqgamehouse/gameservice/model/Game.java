package com.thoriqgamehouse.gameservice.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.springframework.data.redis.core.index.Indexed;

import java.util.List;

@Entity
@Table(name = "game")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Game {
    @Id
    private long id;
    private String name;
    private String description;

    @OneToMany(targetEntity = Event.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "game")
    private List<Event> events;

//    @OneToOne(targetEntity = Player.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, or)
}
