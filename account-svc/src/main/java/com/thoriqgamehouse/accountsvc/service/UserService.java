package com.thoriqgamehouse.accountsvc.service;

import com.thoriqgamehouse.accountsvc.dto.UserRegistration;
import com.thoriqgamehouse.accountsvc.model.User;
import com.thoriqgamehouse.accountsvc.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Optional;
import java.util.List;

@Component
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public Optional<User> findByEmail(String email) throws Exception {
        List<User> users = userRepository.findByEmail(email);
        if(users.isEmpty()) {
            return Optional.empty();
        }
        if(users.size() > 1) {
            throw new Exception("email should be unique!");
        }

        return Optional.of(users.get(0));
    }

    public void saveUser(String email, String password) {
        User user = User.builder()
                .email(email)
                .password(password)
                .build();
        userRepository.save(user);
    }
}
