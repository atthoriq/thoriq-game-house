package com.thoriqgamehouse.accountsvc.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RegistrationResponse  {
    private int statusCode;
    private String message;
}
