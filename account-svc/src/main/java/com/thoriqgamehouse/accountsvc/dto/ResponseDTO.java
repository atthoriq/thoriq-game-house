package com.thoriqgamehouse.accountsvc.dto;


import lombok.Data;

@Data
public class ResponseDTO<T> {
    private String message;
    private String code;
    private T data;
    private T errors;

    public static <T> ResponseDTO<T> ok(T data){
        ResponseDTO<T> res = new ResponseDTO<>();
        res.setData(data);
        res.setMessage("");
        return res;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> ResponseDTO<T> badRequest(T errors, String message) {
        ResponseDTO<T> res = new ResponseDTO<>();
        res.setErrors(errors);
        res.setMessage(message);
        return res;
    }

    public static ResponseDTO general(String message) {
        ResponseDTO res = new ResponseDTO<>();
        res.setMessage(message);
        return res;
    }
}
