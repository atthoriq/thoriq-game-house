package com.thoriqgamehouse.accountsvc.controller;

import com.thoriqgamehouse.accountsvc.dto.*;
import com.thoriqgamehouse.accountsvc.security.JwtIssuer;
import com.thoriqgamehouse.accountsvc.security.UserPrincipal;
import com.thoriqgamehouse.accountsvc.service.AuthService;
import com.thoriqgamehouse.accountsvc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.hibernate.boot.Metadata;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @PostMapping("/auth/login")
    public LoginResponse Login(@RequestBody @Validated LoginRequest request) {
        return authService.attemptLogin(request.getEmail(), request.getPassword());
    }

    @PostMapping("/registration")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseDTO<RegistrationResponse> register(@RequestBody @Validated UserRegistration userRegistration) throws Exception {
        try {
            userService.saveUser(userRegistration.getEmail(), passwordEncoder.encode(userRegistration.getPassword()));
            return ResponseDTO.ok(RegistrationResponse.builder()
                    .statusCode(200)
                    .message("Success.")
                    .build());
        } catch(DataIntegrityViolationException e) {
            System.out.println(e.getClass().getSimpleName());
            return ResponseDTO.badRequest(RegistrationResponse.builder()
                    .statusCode(400)
                    .message("email has been used")
                    .build(), "email has been used");
        }
    }

    @PostMapping("/verify")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseDTO<LoginResponse> VerifyAuth() {
        return ResponseDTO.ok(LoginResponse.builder().build());
    }
}
