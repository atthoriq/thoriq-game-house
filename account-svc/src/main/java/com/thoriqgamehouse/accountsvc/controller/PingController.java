package com.thoriqgamehouse.accountsvc.controller;

import com.thoriqgamehouse.accountsvc.security.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class PingController {
    @GetMapping("/")
    public String greeting() {
        return "Hello Thoriq";
    }

    @GetMapping("/ping")
    public String ping() {
        return "Pong!";
    }

    @GetMapping("/secured")
    public String securedEndpoint(@AuthenticationPrincipal UserPrincipal principal) {
        return "authed endpoint " + principal.getEmail() + " user id "+ principal.getUserId();
    }
}
