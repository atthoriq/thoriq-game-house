# Thoriq Game House
### Planned Project
The project is planned to have 3 services:
- Account Service

This service handles User management including user creation, user authentication, user authorization, and saving user profile.

- Game Service

This service handles Game management including saving game data, event data, and player data.
Every game has multiple events. It is intended because a game can be deployed multiple times through a layer called Event.
Every User (managed by Account Service) has account player for each game. It holds user's metadata for each game.

This service also handles player log. This log is intended to holds any data related to "play" game. 
A play log can consist of play's duration, item used, item collected, etc.
Play logs data is suitable to be stored in unrelational database since the data is not well-defined.

- Leaderboard Service

As per 13 Aug 202, this service is not built yet. This service is to handle Global and Friend leaderboard for each game.
Game service will call this leaderboard service through a Broker (Kafka) after a player submit a play.
The asynchronous process is because to make the leaderboard service reliable, 
and considering leaderboard service holds sensitive data for the user and loss of data is not tolerated.

### Notes
This repository is built for me to exercise my Spring Boot competency. I own every mistakes and bad practices I write in this repository.
I'd like to get feedback for all readers.

---

Regards,

At Thoriq